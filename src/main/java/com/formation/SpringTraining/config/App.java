package com.formation.SpringTraining.config;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.formation.SpringTraining.models.Address;
import com.formation.SpringTraining.models.Employee;
import com.formation.SpringTraining.models.Notes;
import com.formation.SpringTraining.service.AddressService;
import com.formation.SpringTraining.service.EmployeeService;
import com.formation.SpringTraining.service.NotesService;

public class App {
	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class, JPAConfig.class);
		context.refresh();
		
		EmployeeService empService = context.getBean(EmployeeService.class);
		Employee emp1 = new Employee("Frodo", "azerty", "Sacquet", "Frodo", "f.sacquet@ajc.com", "Ring Bearer");
		Employee emp2 = new Employee("Sam", "poiiuyty", "Gamgee", "Sam", "s.gamgee@ajc.com", "Jardinier");
		Employee emp3 = new Employee("Pippin", "qsdfgh", "Touque", "Perigrine", "p.touque@ajc.com", "Accolyte");
		Employee emp4 = new Employee("Merry", "mlkjhg", "Brandebouc", "Meriadoc", "m.brandebouc@ajc.com", "Accolyte");
		
		AddressService addService = context.getBean(AddressService.class);
		Address add1 = new Address(2, "rue des Trucs", "75000", "Hobittown", "Terre du Milieu");
		Address add2 = new Address(15, "rue des Fleurs", "753000", "Hobittown", "Terre du Milieu");
		Address add3 = new Address(2, "rue de la Bouffe", "75000", "Hobittown", "Terre du Milieu");
		Address add4 = new Address(2, "rue du Bidule", "75000", "Hobittown", "Terre du Milieu");
		
		add1.setEmployee(emp1);
		add2.setEmployee(emp2);
		add3.setEmployee(emp3);
		add4.setEmployee(emp4);
		
		NotesService notesService = context.getBean(NotesService.class);
		Notes n1 = new Notes(8, 2, 6, 5, 2015);
		Notes n2 = new Notes(1, 6, 7, 4, 2018);
		Notes n3 = new Notes(5, 5, 5, 5, 2016);
		Notes n4 = new Notes(9, 10, 6, 8, 2018);
		Notes n5 = new Notes(7, 3, 7, 4, 2018);
		Notes n6 = new Notes(10, 0, 4, 10, 2018);
		
		n1.setEmployee(emp1);
		n2.setEmployee(emp1);
		n3.setEmployee(emp2);
		n4.setEmployee(emp2);
		n5.setEmployee(emp3);
		n6.setEmployee(emp4);
		
		empService.createOrUpdateEmployee(emp1);
		empService.createOrUpdateEmployee(emp2);
		empService.createOrUpdateEmployee(emp3);
		empService.createOrUpdateEmployee(emp4);

		addService.createOrUpdateAddress(add1);
		addService.createOrUpdateAddress(add2);
		addService.createOrUpdateAddress(add3);
		addService.createOrUpdateAddress(add4);
		
		notesService.createOrUpdateNote(n1);
		notesService.createOrUpdateNote(n2);
		notesService.createOrUpdateNote(n3);
		notesService.createOrUpdateNote(n4);
		notesService.createOrUpdateNote(n5);
		notesService.createOrUpdateNote(n6);
		
		/*SQL_QUERY
		 * SELECT nom, prenom, moyenne
		 * FROM Employee JOIN notes
		 * ON Employee.employee_id = notes.employee_employeeId
		 * WHERE year = ?
		 * ORDER BY DESC moyenne LIMIT 3;
		 */
		/*List<Employee> empList = empService.findAll();

		Employee empLP = empService.findByLoginAndPassword("King", "azertty");

		for (Employee employee : empList) {
			System.out.println("Login: " + employee.getLogin() + " Password: " + employee.getPassword());
		}

		System.out.println("User nom: " + empLP.getNom());*/
	}
}
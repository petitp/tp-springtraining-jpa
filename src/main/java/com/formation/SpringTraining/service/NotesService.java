package com.formation.SpringTraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.formation.SpringTraining.models.Notes;
import com.formation.SpringTraining.repo.NotesRepository;

@Service
public class NotesService {
	@Autowired
	private NotesRepository notesRepository;
	
	public void createOrUpdateNote(Notes notes)
	{
		this.notesRepository.save(notes);
	}
	
	public void deleteNotes(Notes notes)
	{
		this.notesRepository.delete(notes);
	}
	
	public List<Notes> findAll()
	{
		return this.notesRepository.findAll();
	}
	
	public Page<Notes> findAll(Pageable pageable)
	{
		return this.notesRepository.findAll(pageable);
	}
}

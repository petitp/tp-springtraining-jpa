package com.formation.SpringTraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.formation.SpringTraining.models.Address;
import com.formation.SpringTraining.repo.AddressRepository;


@Service
public class AddressService {
	@Autowired
	private AddressRepository addressRepository;
	
	public void createOrUpdateAddress(Address address)
	{
		this.addressRepository.save(address);
	}
	
	public void deleteAddress(Address address)
	{
		this.addressRepository.delete(address);
	}
	
	public List<Address> findAll()
	{
		return this.addressRepository.findAll();
	}
	
	public Page<Address> findAll(Pageable pageable)
	{
		return this.addressRepository.findAll(pageable);
	}
}

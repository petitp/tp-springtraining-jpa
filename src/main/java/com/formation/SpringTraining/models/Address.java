package com.formation.SpringTraining.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Entity
@Scope("prototype")
@Table(name="address")
public class Address {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="address_id")
	private int id;
	private int numero;
	private String rue;
	
	@Column(name="code_postal")
	private String cp;
	private String ville;
	private String pays;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="address_employeeID", referencedColumnName = "employee_id")
	private Employee employee;
	
	public int getId() 
	{
		return id;
	}
	public void setId(int id) 
	{
		this.id = id;
	}
	
	public int getNumero() 
	{
		return numero;
	}
	public void setNumero(int numero) 
	{
		this.numero = numero;
	}
	
	public String getRue() 
	{
		return rue;
	}
	public void setRue(String rue) 
	{
		this.rue = rue;
	}
	
	public String getCp() 
	{
		return cp;
	}
	public void setCp(String cp) 
	{
		this.cp = cp;
	}
	
	public String getVille() 
	{
		return ville;
	}
	public void setVille(String ville) 
	{
		this.ville = ville;
	}
	
	public String getPays() 
	{
		return pays;
	}
	public void setPays(String pays) 
	{
		this.pays = pays;
	}
	
	public Employee getEmployee() 
	{
		return employee;
	}
	public void setEmployee(Employee employee) 
	{
		this.employee = employee;
	}
	
	public Address() {}
	
	public Address(int numero, String rue, String cp, String ville, String pays) {
		super();
		this.numero = numero;
		this.rue = rue;
		this.cp = cp;
		this.ville = ville;
		this.pays = pays;
		//this.employee_id = employee_id;
	}

}

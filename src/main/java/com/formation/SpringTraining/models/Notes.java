package com.formation.SpringTraining.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Entity
@Scope("prototype")
@Table(name="notes")
public class Notes {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notes_id")
	private int id;
	private float sport;
	private float social;
	private float performance;
	private float assiduite;
	private int year;
	//@Column(name="Moyenne")
	//private float moy;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="employee_employeeId", referencedColumnName="employee_id")
	private Employee employee;
	
	public float getSport() 
	{
		return sport;
	}
	public void setSport(float sport) 
	{
		this.sport = sport;
	}
	
	public float getSocial() 
	{
		return social;
	}
	public void setSocial(float social) 
	{
		this.social = social;
	}
	
	public float getPerformance() 
	{
		return performance;
	}
	public void setPerformance(float performance) 
	{
		this.performance = performance;
	}
	
	public float getAssiduite() 
	{
		return assiduite;
	}
	public void setAssiduite(float assiduite) 
	{
		this.assiduite = assiduite;
	}
	
	public Employee getEmployee() 
	{
		return employee;
	}
	public void setEmployee(Employee employee) 
	{
		this.employee = employee;
	}
	
	public int getYear() 
	{
		return year;
	}
	public void setYear(int year) 
	{
		this.year = year;
	}
	
	public int getId() 
	{
		return id;
	}
	public void setId(int id) 
	{
		this.id = id;
	}
	
	//moy = (sport + social + performance + assiduite)/4;
	
	public Notes(float sport, float social, float performance, float assiduite, int year) {
		super();
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.year = year;
	}
	
	public Notes() {}
}

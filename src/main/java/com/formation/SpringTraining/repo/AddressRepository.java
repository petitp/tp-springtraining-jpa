package com.formation.SpringTraining.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formation.SpringTraining.models.Address;


@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{
	/*public List<Address> findByVille(String n);
	public Page<Address> findByVille(String n, Pageable pageable);*/
	public Page<Address> findAll(Pageable pagegable);
}

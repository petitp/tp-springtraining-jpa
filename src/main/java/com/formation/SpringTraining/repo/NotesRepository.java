package com.formation.SpringTraining.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formation.SpringTraining.models.Notes;


@Repository
public interface NotesRepository extends JpaRepository<Notes, Integer>{
	/*public List<Notes> findByNom(String n);
	public Page<Notes> findByNom(String n, Pageable pageable);*/
	public Page<Notes> findAll(Pageable pagegable);
}
